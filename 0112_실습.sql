use hr;
-- 사원 테이블의 모든 레코드를 조회하시오. --
select * from employees;

-- 사원명과 입사일을 조회하시오. --
select first_name,last_name,hire_date from employees;

-- 사원테이블에 있는 직책의 목록을 조회하시오. --
select job_id from employees;

-- 총 사원수를 구하시오.-- 
select count(*) from employees; 

-- (WHERE 조건) --

-- 부서번호가 10인 사원을 조회하시오. -- #data source가 달라 부서번호 110인 사원 조회함
select * from employees where department_id = 110;

-- 월급여가 2500이상 되는 사원을 조회하시오. --
select * from employees where salary >= 2500;

-- 이름이 'KING'인 사원을 조회하시오. --
select * from employees where last_name = 'King';

-- 사원들 중 이름이 S로 시작하는 사원의 사원번호와 이름을 조회하시오. --
select employee_id,first_name,last_name from employees 
where first_name like 'S%';

-- 사원 이름에 T가 포함된 사원의 사원번호와 이름을 조회하시오. --
select employee_id,first_name,last_name from employees 
where first_name like '%T%'; 

-- 커미션이 300, 500, 1400 인 사원의 사번,이름,커미션을 조회하시오. --
select employee_id,first_name,last_name,commission_pct as c
from employees where c=300 or c=500 or c=1400;

-- 월급여가 1200 에서 3500 사이의 사원의 사번,이름,월급여를 조회하시오. --
select employee_id,first_name,last_name,salary from employees 
where 1200 < salary < 3500;

-- 직급이 매니저이고 부서번호가 30번인 사원의 이름,사번,직급,부서번호를 조회하시오. --
select first_name,last_name,employee_id,job_id,department_id 
from employees where job_id like '%MAN%' and department_id = 30;

-- 부서번호가 30인 아닌 사원의 사번,이름,부서번호를 조회하시오. --
select employee_id,first_name,last_name,department_id 
from employees where not department_id = 30;

-- 커미션이 300, 500, 1400 이 모두 아닌 사원의 사번,이름,커미션을 조회하시오. --
select employee_id,first_name,last_name,commission_pct
from employees where commission_pct not in (300,500,1400);

-- 이름에 S가 포함되지 않는 사원의 사번,이름을 조회하시오. --
select employee_id,first_name,last_name from employees
where first_name or last_name not like '%S%';  

-- 급여가 1200보다 미만이거나 3700 초과하는 사원의 사번,이름,월급여를 조회하시오. --
select employee_id,first_name,last_name,salary from employees
where salary<1200 or salary>3700;

-- 직속상사가 NULL 인 사원의 이름과 직급을 조회하시오. --
select first_name,last_name,job_id from employees 
where manager_id is NULL;