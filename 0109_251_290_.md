```python
# 251
class란 객체의 함수, 변수의 특성과 기능 등을 정의하는 단위이다. 
객체(object)란 class라는 설계도를 기반으로 만든 파이썬의 모든 것이다. 
(예:string,dictionary, tuple 등등)
인스턴스(instance)란 기능이 구체화된 객체를 말한다. 
```


```python
# 252
class Human:
    pass
```


```python
#253
class Human:
    pass
    
areum = Human()
```


```python
#254
class Human:
    def __init__(self):
        print("응애응애")

areum = Human()
areum
```

    응애응애





    <__main__.Human at 0x7fd777404b50>




```python
#255
class Human:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

areum = Human('아름',25,'여자')
print(areum.name)
print(areum.age)
print(areum.sex)
```

    아름
    25
    여자



```python
#256
areum = Human('조아름',25,'여자')
print("이름:",areum.name)
```

    이름: 조아름



```python
#257
class Human:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

    def who(self):
        print("이름:",self.name,",나이:",self.age,",성별:",self.sex)
        
aruem = Human('조아름',25,'여자')
aruem.who
        
```




    <bound method Human.who of <__main__.Human object at 0x7fd777468c10>>




```python
#257
class Human:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

    def who(self):
        print("이름:",self.name,",나이:",self.age,",성별:",self.sex)
        
aruem = Human('조아름',25,'여자')
aruem.who()
        
```

    이름: 조아름 ,나이: 25 ,성별: 여자



```python
#257
class Human:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

    def who(self):
        print("이름: {} 나이: {} 성별 : {}".format(self.name, self.age,self.sex))
        
aruem = Human('조아름',25,'여자')
aruem.who()
```

    이름: 조아름 나이: 25 성별 : 여자



```python
#258
class Human:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

    def setinfo(self):
        input("이름:")
        input("나이:")
        input("성별:")
        print("이름: {} 나이: {} 성별 : {}".format(self.name, self.age,self.sex))

areum = Human('조아름',25,'여자')
areum.setinfo()
```

    이름:조아름
    나이:25
    성별:여자
    이름: 조아름 나이: 25 성별 : 여자



```python
#259
class Human:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

    def __del__(self):
        print("나의 죽음을 알리지 말라")
        
    def setinfo(self):
        input("이름:")
        input("나이:")
        input("성별:")
        print("이름: {} 나이: {} 성별 : {}".format(self.name, self.age,self.sex))
   
        
areum = Human('조아름',25,'여자')
del areum
```

    나의 죽음을 알리지 말라



```python
#260
프린트 함수의 주체(self)가 없기 때문이다.
mystock이라는 객체가 함수를 호출하기 위해서는 print함수의 self가 있어야 한다. 

class OMG : 
    def print(self) :
        print("Oh my god")

myStock = OMG()
myStock.print()


```

    Oh my god



```python
#261
class Stock:
    pass
```


```python
#262
class Stock:
    def __init__(self, theme, code):
        self.theme = theme
        self.code = code
        
삼성 = Stock("삼성전자","005930")
print(삼성.theme)
print(삼성.code)
```

    삼성전자
    005930



```python
#263
class Stock:
    def __init__(self, name, code):
        self.name = name
        self.code = code
    
    def set_name(self, name):
        self.name = name

a = Stock(None,None)
a.set_name('삼성전자')
print(a.name)
```

    삼성전자



```python
#264
class Stock:
    def __init__(self, name, code):
        self.name = name
        self.code = code
    
    def set_name(self, name):
        self.name = name
        
    def set_code(self, code):
        self.code = code

a = Stock(None,None)
a.set_code('005930')
print(a.code)
```

    005930



```python
#265
class Stock:
    def __init__(self, name, code):
        self.name = name
        self.code = code

    def get_name(self, name):
        return self.name
    
    def get_code(self, code):
        return self.code

삼성 = Stock("삼성전자", "005930")
print(삼성.get_name())
print(삼성.get_code())
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    /var/folders/_f/kcby7pyj33z5b0hng6n_s6dh0000gn/T/ipykernel_6240/1105733329.py in <module>
         12 
         13 삼성 = Stock("삼성전자", "005930")
    ---> 14 print(삼성.get_name())
         15 print(삼성.get_code())


    TypeError: get_name() missing 1 required positional argument: 'name'



```python
#265
class Stock:
    def __init__(self, name, code):
        self.name = name
        self.code = code

    def get_name(self):
        return self.name
    
    def get_code(self):
        return self.code

삼성 = Stock("삼성전자", "005930")
print(삼성.get_name())
print(삼성.get_code())
```

    삼성전자
    005930



```python
#266
class Stock:
    def __init__(self, name, code, PER, PBR, 배당수익률):
        self.name = name
        self.code = code
        self.PER = PER
        self.PBR = PBR
        self.배당수익률 = 배당수익률
```


```python
#267
class Stock:
    def __init__(self, name, code, PER, PBR, 배당수익률):
        self.name = name
        self.code = code
        self.PER = PER
        self.PBR = PBR
        self.배당수익률 = 배당수익률
        
    def set_name(self, name):
        self.name = name
        
    def set_code(self, code):
        self.code = code
        
    def get_name(self):
        return self.name
    
    def get_code(self):
        return self.code
    
a = Stock("삼성전자","005930",float(15.79),float(1.33),float(2.83))
print(a.PER)


```

    15.79



```python
#268
class Stock:
    def __init__(self, name, code, PER, PBR, 배당수익률):
        self.name = name
        self.code = code
        self.PER = PER
        self.PBR = PBR
        self.배당수익률 = 배당수익률
        
    def set_name(self, name):
        self.name = name
        
    def set_code(self, code):
        self.code = code
        
    def set_PER(self,PER):
        self.PER = PER
        
    def set_PBR(self,PBR):
        self.PBR = PBR

    def set_dividend(self,배당수익률):
        self.배당수익률 = 배당수익률
        
    def get_name(self):
        return self.name
    
    def get_code(self):
        return self.code
```


```python
#269
class Stock:
    def __init__(self, name, code, PER, PBR, 배당수익률):
        self.name = name
        self.code = code
        self.PER = PER
        self.PBR = PBR
        self.배당수익률 = 배당수익률
        
    def set_name(self, name):
        self.name = name
        
    def set_code(self, code):
        self.code = code
        
    def set_PER(self,PER):
        self.PER = PER
        
    def set_PBR(self,PBR):
        self.PBR = PBR

    def set_dividend(self,배당수익률):
        self.배당수익률 = 배당수익률
        
    def get_name(self):
        return self.name
    
    def get_code(self):
        return self.code
    
a = Stock("삼성전자","005930",float(15.79),float(1.33),float(2.83))
print(a.PER)
a.set_PER(12.75)
print(a.PER)
```

    15.79
    12.75



```python
#270
주식 = []

a = Stock("삼성전자","005930",15.79,1.33,2.83)
b = Stock("현대차","005380",8.70,0.35,4.27)
c = Stock("LG전자","066570",317.34,0.69,1.37)

주식.append(a)
주식.append(b)
주식.append(c)


for i in 주식:
    print(i.code, i.PER)
```

    005930 15.79
    005380 8.7
    066570 317.34



```python
#271
import random
class Account:
    def __init__(self,name,balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"
        num1 = random.randint(0,999)
        num2 = random.randint(0,99)
        num3 = random.randint(0,999999)
        num1 = num1.zfill(3)
        num2 = num2.zfill(2)
        num3 = num3.zfill(6)
        self.account_number = num1+'-'+num2+'-'+num3
        
a = Account('안유진',10000000)
print(a.bank)
print(a.account_number)
```


    ---------------------------------------------------------------------------

    AttributeError                            Traceback (most recent call last)

    /var/folders/_f/kcby7pyj33z5b0hng6n_s6dh0000gn/T/ipykernel_6240/2506089043.py in <module>
         14         self.account_number = num1+'-'+num2+'-'+num3
         15 
    ---> 16 a = Account('안유진',10000000)
         17 print(a.bank)
         18 print(a.account_number)


    /var/folders/_f/kcby7pyj33z5b0hng6n_s6dh0000gn/T/ipykernel_6240/2506089043.py in __init__(self, name, balance)
          9         num2 = random.randint(0,99)
         10         num3 = random.randint(0,999999)
    ---> 11         num1 = num1.zfill(3)
         12         num2 = str(num2).zfill(2)
         13         num3 = str(num3).zfill(6)


    AttributeError: 'int' object has no attribute 'zfill'



```python
#271
import random
class Account:
    def __init__(self,name,balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"
        num1 = random.randint(0,999)
        num2 = random.randint(0,99)
        num3 = random.randint(0,999999)
        num1 = str(num1).zfill(3)
        num2 = str(num2).zfill(2)
        num3 = str(num3).zfill(6)
        self.account_number = num1+'-'+num2+'-'+num3
        
a = Account('안유진',10000000)
print(a.bank)
print(a.account_number)
```

    SC은행
    912-36-174081



```python
#272
import random
class Account:
    account_count = 0
    
    def __init__(self,name,balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"
        num1 = random.randint(0,999)
        num2 = random.randint(0,99)
        num3 = random.randint(0,999999)
        num1 = str(num1).zfill(3)
        num2 = str(num2).zfill(2)
        num3 = str(num3).zfill(6)
        self.account_number = num1+'-'+num2+'-'+num3
        
        Account.account_count += 1
    
a = Account('안유진',1000000)
b = Account('홍길동',1000000)
print(Account.account_count)
```

    2



```python
#273
import random
class Account:
    account_count = 0
    
    def __init__(self,name,balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"
        num1 = random.randint(0,999)
        num2 = random.randint(0,99)
        num3 = random.randint(0,999999)
        num1 = str(num1).zfill(3)
        num2 = str(num2).zfill(2)
        num3 = str(num3).zfill(6)
        self.account_number = num1+'-'+num2+'-'+num3
        
        Account.account_count += 1
    
    @classmethod
    def get_account_num(cls):
        print(cls.account_count)

a = Account('안유진',1000000)
b = Account('홍길동',1000000)

print(a.get_account_num())
```

    2
    None



```python
#274
import random
class Account:
    account_count = 0
    
    def __init__(self,name,balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"
        
        num1 = random.randint(0,999)
        num2 = random.randint(0,99)
        num3 = random.randint(0,999999)
        num1 = str(num1).zfill(3)
        num2 = str(num2).zfill(2)
        num3 = str(num3).zfill(6)
        self.account_number = num1+'-'+num2+'-'+num3
        
        Account.account_count += 1
    
    @classmethod
    def get_account_num(cls):
        print(cls.account_count)
        
    def deposit(self,amount):
        if amount >= 1:
            self.balance += amount

a = Account('안유진',1000000)    
a.deposit(11)
print(a.deposit)
print(a.balance)
```

    <bound method Account.deposit of <__main__.Account object at 0x7fd778eff670>>



```python
#275
import random
class Account:

    account_count = 0

    def __init__(self, name, balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"


        num1 = random.randint(0, 999)
        num2 = random.randint(0, 99)
        num3 = random.randint(0, 999999)

        num1 = str(num1).zfill(3)  
        num2 = str(num2).zfill(2)  
        num3 = str(num3).zfill(6)  
        self.account_number = num1 + '-' + num2 + '-' + num3  # 001-01-000001
        Account.account_count += 1

    @classmethod
    def get_account_num(cls):
        print(cls.account_count)  

    def deposit(self, amount):
        if amount >= 1:
            self.balance += amount

    def withdraw(self, amount):
        if self.balance > amount:
            self.balance -= amount

a = Account('안유진',1000000)    
a.deposit(100)
a.withdraw(90)
print(a.balance)
```

    1000010



```python
#276
import random
class Account:

    account_count = 0

    def __init__(self, name, balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"


        num1 = random.randint(0, 999)
        num2 = random.randint(0, 99)
        num3 = random.randint(0, 999999)

        num1 = str(num1).zfill(3)  
        num2 = str(num2).zfill(2)  
        num3 = str(num3).zfill(6)  
        self.account_number = num1 + '-' + num2 + '-' + num3  # 001-01-000001
        Account.account_count += 1

    @classmethod
    def get_account_num(cls):
        print(cls.account_count)  

    def deposit(self, amount):
        if amount >= 1:
            self.balance += amount

    def withdraw(self, amount):
        if self.balance > amount:
            self.balance -= amount
        
    def display_info(self):
        print("은행이름:",self.bank)
        print("예금주:", self.name)
        print("계좌번호:", self.account_number)
        print("잔고:", self.balance)
        
a = Account("파이썬", 100000)
a.display_info()

```

    은행이름: SC은행
    예금주: 파이썬
    계좌번호: 181-24-614407
    잔고: 100000



```python
#277
import random
class Account:

    account_count = 0

    def __init__(self, name, balance):
        self.name = name
        self.balance = balance
        self.bank = "SC은행"


        num1 = random.randint(0, 999)
        num2 = random.randint(0, 99)
        num3 = random.randint(0, 999999)

        num1 = str(num1).zfill(3)  
        num2 = str(num2).zfill(2)  
        num3 = str(num3).zfill(6)  
        self.account_number = num1 + '-' + num2 + '-' + num3  # 001-01-000001
        Account.account_count += 1

    @classmethod
    def get_account_num(cls):
        print(cls.account_count)  

    def deposit(self, amount):
        if amount >= 1:
            self.balance += amount

            self.deposit_count += 1
            if self.deposit_count % 5 == 0:        
                self.balance = (self.balance * 1.01)

    def withdraw(self, amount):
        if self.balance > amount:
            self.balance -= amount
        
    def display_info(self):
        print("은행이름:",self.bank)
        print("예금주:", self.name)
        print("계좌번호:", self.account_number)
        print("잔고:", self.balance)
        
a = Account("파이썬", 100000)
a.deposit(10000)
a.deposit(10000)
a.deposit(10000)
a.deposit(5000)
a.deposit(5000)
print(a.balance)



```


    ---------------------------------------------------------------------------

    AttributeError                            Traceback (most recent call last)

    /var/folders/_f/kcby7pyj33z5b0hng6n_s6dh0000gn/T/ipykernel_6240/3732940123.py in <module>
         44 
         45 a = Account("파이썬", 100000)
    ---> 46 a.deposit(10000)
         47 a.deposit(10000)
         48 a.deposit(10000)


    /var/folders/_f/kcby7pyj33z5b0hng6n_s6dh0000gn/T/ipykernel_6240/3732940123.py in deposit(self, amount)
         29             self.balance += amount
         30 
    ---> 31             self.deposit_count += 1
         32             if self.deposit_count % 5 == 0:
         33                 self.balance = (self.balance * 1.01)


    AttributeError: 'Account' object has no attribute 'deposit_count'

